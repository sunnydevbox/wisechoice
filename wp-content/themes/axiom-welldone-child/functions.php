<?php
/**
 * Child-Theme functions and definitions
 */
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

add_filter( 'woocommerce_product_tabs', 'remove_brands_tab');
function remove_brands_tab($tabs) {

 unset($tabs['pwb_tab']);

 return $tabs;
}

function woocommerce_custom_fee( ) {

	if ( ( is_admin() && ! defined( 'DOING_AJAX' ) ) || ! is_checkout() )
		return;

	$chosen_gateway = WC()->session->chosen_payment_method;

	// $fee = get_option( 'woocommerce_payment_add_on', true );
	// or calculate your $fee with all the php magic...
    // echo (get_option( 'woocommerce_payment_add_on', true ) / 100);
        $fee = WC()->cart->cart_contents_total * (get_option( 'woocommerce_payment_add_on', true ) / 100); // sample computation for getting 2.5% of the cart total.

	if ( $chosen_gateway == 'paypal' ) { //test with paypal method
		WC()->cart->add_fee( 'Paypal fee', $fee, false, '' );
	}
}
add_action( 'woocommerce_cart_calculate_fees','woocommerce_custom_fee' );

function cart_update_script() {
    if (is_checkout()) :
    ?>
    <script>
		jQuery( function( $ ) {

			// woocommerce_params is required to continue, ensure the object exists
			if ( typeof woocommerce_params === 'undefined' ) {
				return false;
			}

			$checkout_form = $( 'form.checkout' );

			$checkout_form.on( 'change', 'input[name="payment_method"]', function() {
					$checkout_form.trigger( 'update' );
			});


		});
    </script>
    <?php
    endif;
}
add_action( 'wp_footer', 'cart_update_script', 999 );




//  test

add_filter('woocommerce_general_settings', 'general_settings_shop_phone');
function general_settings_shop_phone($settings) {
    $key = 0;
    // echo "<pre>";
    // var_dump($settings);
    // echo "<pre/>";
    // echo 'asdasdasd '.get_option( 'woocommerce_payment_add_on', true );
    foreach( $settings as $values ){
        $new_settings[$key] = $values;
        $key++;

        // echo $values['id']."<br/>";
        // if($values['id'] == 'woocommerce_price_num_decimals'){
        //     echo "<pre>";
        //     var_dump($values);
        //     echo "<pre/>";
        // }
        // Inserting array just after the post code in "Store Address" section

        if($values['id'] == 'woocommerce_price_num_decimals'){
            $new_settings[$key] = array(
                'title'    => __('Extra Field for additional extra payment when using paypal (%)'),
                'desc'     => __('Optional extra charge for paypal (%)'),
                'id'       => 'woocommerce_payment_add_on', // <= The field ID (important)
                'default'  => '10',
                'type'     => 'number',
                'css'      => 'width:100px',
                'desc_tip' => true, // or false
            );
            $key++;
        }
    }
    return $new_settings;
}

// change text in product looop
add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
/**
 * custom_woocommerce_template_loop_add_to_cart
*/
function custom_woocommerce_product_add_to_cart_text() {
	global $product;

	$product_type = $product->product_type;

	switch ( $product_type ) {
		case 'external':
			return __( 'Buy product', 'woocommerce' );
		break;
		case 'grouped':
			return __( 'View products', 'woocommerce' );
		break;
		case 'simple':
			return __( 'Add to cart', 'woocommerce' );
		break;
		case 'variable':
			return __( 'View', 'woocommerce' );
		break;
		default:
			return __( 'Read more', 'woocommerce' );
	}

}


// add_filter( 'woocommerce_bacs_account_fields', 'filter_function_name_474', 10, 2 );
// function filter_function_name_474( $array, $order_id ){
//
//     if($array) : foreach ($array as $key => $details) {
//
//
//     } endif;
//
// 	return $array;
// }


?>
